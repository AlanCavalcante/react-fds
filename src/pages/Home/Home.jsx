import React from 'react'
import {Link} from 'react-router-dom'
import './Home.css'
import ImgIntroducao from '../../assets/pig-standing.svg'
import ImgMeta from '../../assets/goals.svg'
import ImgGridOne from '../../assets/how-to-1.svg'
import ImgGridTwo from '../../assets/how-to-2.svg'
import ImgGridThree from '../../assets/how-to-3.svg'

function Home(){
    return(
        <>
            <div className='menu'>
                        <Link to='/cotacao'>Cotacao</Link>
            </div>
            <section className='introducao Home'>
                <div className='side-left'>
                    <h1>Ping Blank</h1>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus. 
                    </p>
                    
                    <button className='btn-padrao'><Link to='/soon'>nova meta</Link></button>

                </div>
                <div className='side-right'>
                    <img src={ImgIntroducao} alt="Porco segurando Moeda"/>
                </div>
            </section>

            <section className='como-usar'>
                <h1>Como usar</h1>
                <div className="cards">
                    <div className='grid-1 Home'>
                        <img src={ImgGridOne} alt="Senhora na cadeira"/>
                        <h2>Defina uma meta</h2>
                        <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
                    </div>

                    <div className='grid-1'>
                        <img src={ImgGridTwo} alt="Senhora na cadeira"/>
                        <h2>Defina uma meta</h2>
                        <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
                    </div>

                    <div className='grid-1'>
                        <img src={ImgGridThree} alt="Senhora na cadeira"/>
                        <h2>Defina uma meta</h2>
                        <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
                    </div>
                </div>
            </section>

            <section className='meta Home'>
                <div className='side-left'>
                    <h1>Já tem uma meta?</h1>
                    <p>Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam nunc lacus, commodo hendrerit commodo pulvinar, suscipit eget ipsum. Nulla at sem ex.</p>
                    <button className='btn-padrao'><Link to='/soon'>ver meta</Link></button>
                    <h1>Não tem? Comece agora mesmo</h1>
                </div>
                <div className='side-right'>
                    <img src={ImgMeta} alt="menino guardando moeda no porco" className='MetaImg'/>
                    <button className='btn-padrao' ><Link to='/soon'>nova meta</Link></button>
                </div>
            </section>
        </>
    );
}

export default Home