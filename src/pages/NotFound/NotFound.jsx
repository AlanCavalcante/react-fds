import React from 'react'
import './NotFound.css'
import {Link} from 'react-router-dom'
import ImgNotFound from '../../assets/ping_confuso.svg'

function NotFound(){
    return(
        <main className='NotFound'>
            <h1>Error 404</h1>
            <img src={ImgNotFound} alt="Porco com duvidas"/>
            <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
            <button type="button"> <Link to='/'>Voltar</Link></button>
        </main>
    );
}

export default NotFound;