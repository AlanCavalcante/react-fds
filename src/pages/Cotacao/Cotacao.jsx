import React,{useEffect, useState} from 'react'
import {Link} from 'react-router-dom'
import { moedaApi } from '../../services/moedaAPI'
import ImgConfuso from '../../assets/ping_confuso.svg'
import './Cotacao.css'

function Cotacao(){
    const [moeda, setMoeda] = useState([])
    useEffect(()=>{
        moedaApi(setMoeda)
    },[])


    return(
        <>
            <main>
                <div className='menu'>
                    <Link to='/'>Entrar</Link>
                </div>
                <h1>Cotação de Moedas</h1>
                {
                    moeda.map(din =>{
                        return(
                            <div className='cotacao'>
                                <div className='cot-left'>
                                    <h3>{din.name}</h3>
                                    <h1>{din.code}</h1>
                                </div>
                                <div className='cot-right'>
                                    <h2>Máxima: {din.high}</h2>
                                    <h2>Minima: {din.low}</h2>
                                    <p>Atualizado em {din.create_date}</p>
                                </div>
                            </div>
                        )
                    })
                }
            </main>
            <img src={ImgConfuso} alt="Porco Confuso" className='ImgCotacao'/>
        </>
    )
}

export default Cotacao