import React from 'react'
import ImgSoon from '../../assets/soon.svg'
import '../NotFound/NotFound.css'
import Footer from '../../common/Footer/Footer.jsx'

function Soon(){
    return(
        <main className='NotFound'>
            <h1>Em construção</h1>
            <img src={ImgSoon} alt="Em manutenção"/>
            <p>Parece que essa página ainda não foi implementada... 
Tente novamente mais tarde!</p>
            <button><a href="#">voltar</a></button>
            <Footer></Footer>
        </main>
    );
}

export default Soon;
