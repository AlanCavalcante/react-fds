import React from 'react'
import './Header.css'

function Header(){
    return(
        <header>
            <input type="checkbox" id="nav-menu2"/>
            <label for="nav-menu2">
                <div id="nav-icon2">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </label>
        </header>
    );
}

export default Header