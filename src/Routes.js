import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Cotacao from './pages/Cotacao/Cotacao'
import Footer from './common/Footer/Footer'
import Home from './pages/Home/Home'
import NotFound from './pages/NotFound/NotFound'
import Soon from './pages/Soon/Soon'
import Header from './common/Header/Header'

function Routes(){
    return(
        <BrowserRouter>
        <Header></Header>
        <Switch>
            <Route path='/cotacao'>
                <Cotacao></Cotacao>
            </Route>
            <Route exact path='/'>
                <Home></Home>
            </Route>
            <Route path='/soon'>
                <Soon></Soon>
            </Route>
            <Route path=''>
                <NotFound></NotFound>
            </Route>
        </Switch>
        <Footer></Footer>
    </BrowserRouter>
    )
}

export default Routes