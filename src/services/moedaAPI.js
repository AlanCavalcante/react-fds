import axios from 'axios';

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/"
})

async function moedaApi(setEstado){
    await api.get('/json/all').then(res=>{
        let lista = [];
        Object.keys(res.data).forEach(key=>{
            lista.push(res.data[key]);
        });
        setEstado(lista);
        
    }).catch(err=>{
        console.log(err);
    });
}

export {moedaApi}